import os
import smtplib

example_html = """
<html>
	<body>
		<table style="width:100%">
			<tr><th>Servicename</th><th>TestResult</th><th>HTTP Response</th><th>Process Time(in Seconds)</th></tr>
			<tr><td>https://www.google.com</td><td>Pass</td><td>200</td><td>0.123</td></tr>
			<tr><td>https://www.google.com</td><td>Pass</td><td>200</td><td>0.123</td></tr>	
		</table>
	</body>
</html>
"""

URL_LIST = ["https://www.google.com", "https://wwww.yahoo.com", "test.corp.sdasak", "https://www.apple.com",
            "https://www.doesntwork.casa"]

if os.path.isfile('report.txt'):
    os.remove("report.txt")


def alertmail(statusMsg):
    sender = 'sender@gmail.com' #sender email id
    password = 'dsaicdsa' #sender password

    recipient = 'receiever@gmail.com'

    subject = "Service Alert!!"
    body = statusMsg
    body = "" + body + ""

    headers = ["From: " + sender, "Subject: " + subject, "To: " + recipient, "MIME-Version: 1.0",
               "Content-Type: text/html"]
    headers = "\r\n".join(headers)

    session = smtplib.SMTP('smtp.gmail.com:587')
    session.ehlo()
    session.starttls()
    session.login(sender, password)
    session.sendmail(sender, recipient, headers + "\r\n\r\n" + body)
    session.quit()


def getstatus():
    """

    :rtype: send Email in HTML table format.
    """
    TableHeader = "<html><head><title>ServiceStatus</title><table border=2><tr><th>ServiceName</th><th>TestResult</th><th>HTTP Response</th><th>Process Time(in Seconds)</th></tr>"
    report_file = open("report.txt", "a")
    report_file.write(TableHeader)
    try:
        for URL in URL_LIST:
            print "Checking Connection for " + str(URL)
            serviceStatus = os.popen('curl -I ' + str(URL) + ' 2>/dev/null | head -n 1 | cut -d$' ' -f2 ').read()
            response_time = os.popen('curl -s -w "%{time_total}\n" -o /dev/null ' + str(URL)).read()
            if "HTTP/1.1 200 OK" in serviceStatus:
                response = "200"
                result = "Pass"
                print str(URL) + " resonded with " + str(serviceStatus) + " in " + str(response_time)
                data = "<tr><td>" + str(URL) + "</td><td bgcolor=green>" + str(result) + "</td><td>" + str(
                    response) + "</td><td>" + str(response_time) + "</td></tr>"
                report_file.write(data)
            else:
                response = str(serviceStatus)
                result = "Fail"
                print str(URL) + " responded with " + str(response) + " in " + str(response_time)
                data = "<tr><td>" + str(URL) + "</td><td bgcolor=red>" + str(result) + "</td><td>" + str(
                    response) + "</td><td>" + str(response_time) + "</td></tr>"
                report_file.write(data)
        endReport = "</table></head></html>"
        report_file.write(endReport)
        report_file.close()
        readfile = open("report.txt", "r")
        readMsg = readfile.read()
        # print readMsg
        alertmail(readMsg)
        readfile.close()
    except smtplib.SMTPAuthenticationError:
        print "Invalid Email or Invalid username/password!"
    else:
        print "Email sent!"


if __name__ == "__main__":
    getstatus()
